package com.localfootball.geo.external.openstreetmap.model

import com.fasterxml.jackson.annotation.JsonProperty

data class OpenStreetMapReverseGeocodingResponse(
    val address: OpenStreetMapAddress
)

data class OpenStreetMapAddress(
    private val houseNumber: Int?,
    private val road: String?,
    private val suburb: String?,
    private val village: String?,
    private val county: String?,
    private val town: String?,
    private val city: String?,
    @field: JsonProperty("country_code")
    val countryCode: String
) {
    fun street() = road?.let { road ->
        houseNumber?.let { "$road $it" } ?: road
    }

    fun city() = city ?: town ?: village ?: suburb
}