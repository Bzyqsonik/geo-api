package com.localfootball.geo.external.openstreetmap.client

import com.localfootball.geo.external.openstreetmap.model.OpenStreetMapReverseGeocodingResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import org.springframework.web.util.UriComponentsBuilder.fromUriString
import reactor.core.publisher.Mono

@Component
class OpenStreetMapGeocodingApiClient(
    private val webClient: WebClient,
    @Value("\${external.service.openStreetMap.url}") private val openStreetMapUrl: String
) {

    fun findAddressByCoordinates(latitude: Double, longitude: Double): Mono<OpenStreetMapReverseGeocodingResponse> =
        webClient
            .get()
            .uri(
                fromUriString(openStreetMapUrl)
                    .queryParam("lat", latitude)
                    .queryParam("lon", longitude)
                    .queryParam("format", "json")
                    .build()
                    .toUri()
            )
            .retrieve()
            .bodyToMono()
}