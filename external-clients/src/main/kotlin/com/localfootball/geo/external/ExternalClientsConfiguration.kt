package com.localfootball.geo.external

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders.ACCEPT
import org.springframework.http.HttpHeaders.CONTENT_ENCODING
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.reactive.function.client.WebClient

@Configuration
@ComponentScan
class ExternalClientsConfiguration {

    @Bean
    fun webClient() = WebClient.builder()
        .defaultHeader(ACCEPT, APPLICATION_JSON_VALUE)
        .defaultHeader(CONTENT_ENCODING, APPLICATION_JSON_VALUE)
        .build()
}