package com.localfootball.geo.app.service

import com.localfootball.geo.app.exception.InvalidCountryException
import com.localfootball.geo.app.model.Address
import com.localfootball.geo.app.model.Coordinates
import com.localfootball.geo.external.openstreetmap.client.OpenStreetMapGeocodingApiClient
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.Mono.error
import reactor.core.publisher.switchIfEmpty

private const val POLAND_COUNTRY_CODE = "pl"

@Service
class FindAddressService(private val openStreetMapGeocodingApiClient: OpenStreetMapGeocodingApiClient) {

    fun findByCoordinates(coordinates: Coordinates): Mono<Address> =
        openStreetMapGeocodingApiClient.findAddressByCoordinates(coordinates.latitude, coordinates.longitude)
            .filter { POLAND_COUNTRY_CODE == it.address.countryCode }
            .switchIfEmpty { error(InvalidCountryException("Requested coordinates are not in Poland!")) }
            .map {
                Address(
                    street = it.address.street(),
                    city = it.address.city()
                )
            }
}