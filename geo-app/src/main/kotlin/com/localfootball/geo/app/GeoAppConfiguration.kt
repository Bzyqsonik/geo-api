package com.localfootball.geo.app

import com.localfootball.geo.external.ExternalClientsConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@ComponentScan
@Import(ExternalClientsConfiguration::class)
class GeoAppConfiguration