package com.localfootball.geo.app.model

data class Coordinates(
    val latitude: Double,
    val longitude: Double
)