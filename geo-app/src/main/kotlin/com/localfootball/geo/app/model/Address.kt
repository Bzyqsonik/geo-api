package com.localfootball.geo.app.model

data class Address(
    val street: String?,
    val city: String?
)