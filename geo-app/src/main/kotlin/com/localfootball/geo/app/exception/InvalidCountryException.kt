package com.localfootball.geo.app.exception

import java.lang.RuntimeException

data class InvalidCountryException(override val message: String): RuntimeException(message)