package com.localfootball.geo.api.controller

import com.localfootball.geo.api.exception.InvalidCoordinateException
import com.localfootball.geo.app.model.Coordinates
import reactor.core.publisher.Mono

private const val MIN_COORDINATE_VALUE = -180
private const val MAX_COORDINATE_VALUE = 180
private const val INVALID_COORDINATE_MESSAGE = "Missing or invalid coordinate"

fun prepareCoordinates(latitude: Double, longitude: Double) = Mono.just(
    Coordinates(
        latitude.validated("latitude"),
        longitude.validated("longitude")
    )
)

private fun Double.validated(coordinateName: String) =
    if (this < MIN_COORDINATE_VALUE || this > MAX_COORDINATE_VALUE)
        throw InvalidCoordinateException("$INVALID_COORDINATE_MESSAGE: $coordinateName")
    else this