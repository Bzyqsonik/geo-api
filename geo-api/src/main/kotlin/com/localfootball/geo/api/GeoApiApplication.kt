package com.localfootball.geo.api

import com.localfootball.geo.app.GeoAppConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import

@SpringBootApplication
@Import(GeoAppConfiguration::class)
class GeoApiApplication

fun main(args: Array<String>) {
    runApplication<GeoApiApplication>(*args)
}