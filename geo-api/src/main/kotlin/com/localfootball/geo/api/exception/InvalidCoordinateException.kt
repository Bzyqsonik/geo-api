package com.localfootball.geo.api.exception

import java.lang.RuntimeException

internal data class InvalidCoordinateException(override val message: String): RuntimeException(message)