package com.localfootball.geo.api.model

data class ErrorResponse(
    val message: String
)