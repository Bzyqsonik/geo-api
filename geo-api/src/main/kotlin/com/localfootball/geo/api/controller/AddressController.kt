package com.localfootball.geo.api.controller

import com.localfootball.geo.api.exception.InvalidCoordinateException
import com.localfootball.geo.api.model.ErrorResponse
import com.localfootball.geo.app.exception.InvalidCountryException
import com.localfootball.geo.app.model.Address
import com.localfootball.geo.app.service.FindAddressService
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/addresses")
internal class AddressController(private val findAddressService: FindAddressService) {

    @GetMapping
    fun findAddressByCoordinates(@RequestParam latitude: Double, @RequestParam longitude: Double): Mono<Address> =
        prepareCoordinates(latitude, longitude)
            .flatMap(findAddressService::findByCoordinates)

    @ExceptionHandler(InvalidCoordinateException::class)
    @ResponseStatus(BAD_REQUEST)
    fun handleInvalidCoordinateException(ex: InvalidCoordinateException) = ErrorResponse(ex.message.also { println(ex.message) })

    @ExceptionHandler(InvalidCountryException::class)
    @ResponseStatus(BAD_REQUEST)
    fun handleInvalidCoordinateException(ex: InvalidCountryException) = ErrorResponse(ex.message.also { println(ex.message) })
}