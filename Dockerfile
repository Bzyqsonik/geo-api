FROM openjdk:8-jdk-alpine

COPY geo-api/build/libs/geo-api-1.0.jar geo-api.jar

ENV app_name "Geo API"

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "-Xms384m",  "-Xmx768m", "geo-api.jar"]